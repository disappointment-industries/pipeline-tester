FROM scratch

ADD x86_64 /app
ENTRYPOINT ["/app"]
